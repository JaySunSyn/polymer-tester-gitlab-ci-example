#Example how to run polymer tests on gitlab-ci

Main problem was to run chrome without sandbox [blog post](http://blog.kupriyanov.com/2017/02/solvederror-response-status-6-selenium.html)
 - Example [.gitlab-ci.yml](.gitlab-ci.yml)
 - Simplified example [.gitlab-ci-custom.yml](.gitlab-ci-custom.yml) with using [custom Docker image](https://hub.docker.com/r/printminion/polymer-tester/)


Test element

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your application locally.

## Viewing Your Application

```
$ polymer serve
```

## Building Your Application

```
$ polymer build
```

This will create a `build/` folder with `bundled/` and `unbundled/` sub-folders
containing a bundled (Vulcanized) and unbundled builds, both run through HTML,
CSS, and JS optimizers.

You can serve the built versions by giving `polymer serve` a folder to serve
from:

```
$ polymer serve build/bundled
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
